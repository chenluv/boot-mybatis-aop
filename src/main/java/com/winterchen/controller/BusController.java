package com.winterchen.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.winterchen.dao.ParkingMapper;
import com.winterchen.model.Parking;
import io.itit.itf.okhttp.FastHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/16.
 */
@RestController
@RequestMapping(value = "/bus")
public class BusController extends BaseController {


    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public Object addParking() throws Exception {
        String washUrl = url + "3904f7f2754b42a09214bd4e1edceb12";
        String encodedText = encodedText();
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("lineid", "20000005");
        bodyJson.put("dir", "1");
        String strJson = bodyJson.toString();
        String resp = FastHttpClient.post().addHeader("Content-Type", "application/json").addHeader("Authorization", "Basic " + encodedText).url(washUrl).body(strJson).build().execute().string();
//        JSONObject jsonObject = JSONObject.parseObject(resp);
//        List<Parking> parkings = new ArrayList<Parking>();
//        JSONArray data = jsonObject.getJSONArray("body");
        return resp;
//        return data.getJSONObject(5);
//        if (data.size() > 0) {
//            for (int i = 0; i < data.size(); i++) {
//                JSONObject json = data.getJSONObject(i);
//                Parking parking = new Parking();
//                parking.setName(json.getString("NAME"));
//                parking.setNumber(json.getString("OPEN_NUM"));
//                parking.setAddress(json.getString("ADDRESS"));
//                parking.setLongitude(json.getString("LONGITUDE"));
//                parking.setLatitude(json.getString("LATITUDE"));
//                parkings.add(parking);
//            }
//        }
//        return parkings;
//        int isSuccess = parkingMapper.insertList(parkings);
////////        Washroom isSuccess=Mapper.selectByPrimaryKey(1);
//        return isSuccess;
    }

}
