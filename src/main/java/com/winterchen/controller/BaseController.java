package com.winterchen.controller;

import com.alibaba.fastjson.JSONObject;

import io.itit.itf.okhttp.FastHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

/**
 * Created by Administrator on 2017/8/16.
 */

public class BaseController {


    @Value("${haian.url}")
    protected String url ;


    public String encodedText() throws Exception {
        String idKey = "1542088056692:bb93136993e24cbebebe0700046cbd88";
        final Base64.Encoder encoder = Base64.getEncoder();
        final byte[] textByte = idKey.getBytes("UTF-8");
        final String encodedText = encoder.encodeToString(textByte);
        return  encodedText;
    }

}
