package com.winterchen.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.winterchen.config.DS;
import com.winterchen.dao.WashroomMapper;
import com.winterchen.impl.Washroommpl;
import com.winterchen.model.Washroom;
import io.itit.itf.okhttp.FastHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * Created by Administrator on 2017/8/16.
 */
@RestController
@RequestMapping(value = "/washroom")



public class WashroomController extends BaseController {

//    @Autowired
//    private Washroom washroom;


    @Autowired
    private WashroomMapper washroomMapper;


    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @DS("datasource1")
    public Object addWashroom() throws Exception {
        String washUrl = url + "545a8ac1e1644d7d90f74c5d11dd7151";
        String encodedText = encodedText();
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("onset", "0");
        bodyJson.put("offset", "500000");
        String strJson = bodyJson.toString();
        String resp = FastHttpClient.post().addHeader("Content-Type", "application/json").addHeader("Authorization", "Basic " + encodedText).url(washUrl).body(strJson).build().execute().string();
        JSONObject jsonObject = JSONObject.parseObject(resp);
        List<Washroom> washrooms = new ArrayList<Washroom>();
        JSONArray data = jsonObject.getJSONArray("body");
        return jsonObject;
//        return data.getJSONObject(5);
//        if (data.size() > 0) {
//            for (int i = 0; i < data.size(); i++) {
//                JSONObject json = data.getJSONObject(i);
//                Washroom washroom = new Washroom();
//                washroom.setName(json.getString("WC_NAME"));
//                washroom.setPic(json.getString("PIC"));
//                washroom.setAddress(json.getString("ADDRESS"));
//                washroom.setLongitude(json.getString("LONGITUDE"));
//                washroom.setLatitude(json.getString("LATITUDE"));
//                washrooms.add(washroom);
//            }
//        }
//        int isSuccess = washroomMapper.insertList(washrooms);
////////        Washroom isSuccess=Mapper.selectByPrimaryKey(1);
//        return isSuccess;
    }





}
