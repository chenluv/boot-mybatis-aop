package com.winterchen.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.winterchen.config.DS;
import com.winterchen.dao.ParkingMapper;
import com.winterchen.dao.WashroomMapper;
import com.winterchen.model.Parking;
import com.winterchen.model.Washroom;
import io.itit.itf.okhttp.FastHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/16.
 */
@RestController
@RequestMapping(value = "/parking")
public class ParkingController extends BaseController {

//    @Autowired
//    private Washroom washroom;


    @Autowired
    private ParkingMapper parkingMapper;


    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @DS("datasource1")
    public Object addParking() throws Exception {
        String washUrl = url + "30f0ec00f8824815b79e7b5d7cd2f83b";
        String encodedText = encodedText();
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("onset", "0");
        bodyJson.put("offset", "500000");
        String strJson = bodyJson.toString();
        String resp = FastHttpClient.post().addHeader("Content-Type", "application/json").addHeader("Authorization", "Basic " + encodedText).url(washUrl).body(strJson).build().execute().string();
        JSONObject jsonObject = JSONObject.parseObject(resp);
        List<Parking> parkings = new ArrayList<Parking>();
        JSONArray data = jsonObject.getJSONArray("body");
        return jsonObject;
//        return data.getJSONObject(5);
//        if (data.size() > 0) {
//            for (int i = 0; i < data.size(); i++) {
//                JSONObject json = data.getJSONObject(i);
//                Parking parking = new Parking();
//                parking.setName(json.getString("NAME"));
//                parking.setNumber(json.getString("OPEN_NUM"));
//                parking.setAddress(json.getString("ADDRESS"));
//                parking.setLongitude(json.getString("LONGITUDE"));
//                parking.setLatitude(json.getString("LATITUDE"));
//                parkings.add(parking);
//            }
//        }
//        return parkings;
//        int isSuccess = parkingMapper.insertList(parkings);
////////        Washroom isSuccess=Mapper.selectByPrimaryKey(1);
//        return isSuccess;
    }

}
