package com.winterchen.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.winterchen.config.DS;
import com.winterchen.dao.HospitalMapper;
import com.winterchen.dao.WashroomMapper;
import com.winterchen.model.Hospital;
import com.winterchen.model.Washroom;
import io.itit.itf.okhttp.FastHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/16.
 */
@RestController
@RequestMapping(value = "/hospital")
public class HospitalController extends BaseController {


    @Autowired
    private HospitalMapper hospitalMapper;


    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @DS("datasource1")
    public Object addHospital() throws Exception {
        String washUrl = url + "7092057a5d4e47e9bb5f52ae61c7deb9";
        String encodedText = encodedText();
        JSONObject bodyJson = new JSONObject();
        bodyJson.put("onset", "0");
        bodyJson.put("offset", "500000");
        String strJson = bodyJson.toString();
        String resp = FastHttpClient.post().addHeader("Content-Type", "application/json").addHeader("Authorization", "Basic " + encodedText).url(washUrl).body(strJson).build().execute().string();
        JSONObject jsonObject = JSONObject.parseObject(resp);
        List<Hospital> hospitals = new ArrayList<Hospital>();
        JSONArray data = jsonObject.getJSONArray("body");
        return jsonObject;
//        return jsonObject;
//        return data.getJSONObject(5);
//        if (data.size() > 0) {
//            for (int i = 0; i < data.size(); i++) {
//                JSONObject json = data.getJSONObject(i);
//                Hospital hospital = new Hospital();
//                hospital.setName(json.getString("INST_NAME"));
//                hospital.setLevel(json.getString("HOSPITAL_LEVEL"));
//                hospitals.add(hospital);
//            }
//        }
//        int isSuccess = hospitalMapper.insertList(hospitals);
////////        Washroom isSuccess=Mapper.selectByPrimaryKey(1);
//        return isSuccess;
    }

}
