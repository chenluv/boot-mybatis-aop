package com.winterchen.dao;

import com.winterchen.config.DS;
import com.winterchen.model.Washroom;
import org.springframework.stereotype.Service;

import java.util.List;

public interface WashroomMapper {

//    int deleteByPrimaryKey(Integer id);

//    int insert(Washroom record);
@DS("datasource1")
    int insertList(List<Washroom> list);

//    int insertSelective(Washroom record);
@DS("datasource1")
    Washroom selectByPrimaryKey(Integer id);

//    int updateByPrimaryKeySelective(Washroom record);

//    int updateByPrimaryKey(Washroom record);
}