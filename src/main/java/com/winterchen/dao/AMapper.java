package com.winterchen.dao;

import com.winterchen.model.A;

public interface AMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(A record);

    int insertSelective(A record);

    A selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(A record);

    int updateByPrimaryKey(A record);
}