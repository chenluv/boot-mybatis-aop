package com.winterchen.dao;


import com.winterchen.config.DS;
import com.winterchen.model.Hospital;

import java.util.List;


public interface HospitalMapper {

//    int deleteByPrimaryKey(Integer id);

    @DS("datasource2")
    int insert(Hospital record);

//    int insertSelective(Hospital record);

    @DS("datasource2")
    int insertList(List<Hospital> list);

    @DS("datasource2")
    Hospital selectByPrimaryKey(Integer id);

//    int updateByPrimaryKeySelective(Hospital record);

//    int updateByPrimaryKey(Hospital record);
}