package com.winterchen.dao;

import com.winterchen.config.DS;
import com.winterchen.model.Parking;

import java.lang.annotation.Target;
import java.util.List;


public interface ParkingMapper {

//    int deleteByPrimaryKey(Integer id);

    @DS("datasource1")
    int insert(Parking record);

//    int insertSelective(Parking record);

    @DS("datasource1")
    int insertList(List<Parking> list);

    @DS("datasource1")
    Parking selectByPrimaryKey(Integer id);

//    int updateByPrimaryKeySelective(Parking record);

//    int updateByPrimaryKey(Parking record);
}